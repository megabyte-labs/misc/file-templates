FROM alpine:3

ENV container docker

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]
RUN apk --no-cache add --virtual build-dependencies \
      py3-pip~=20 \
  && apk --no-cache add \
      python3~=3 \
  && pip3 install --no-cache-dir \
      "flake8==3.*" \
  && apk del build-dependencies \
  && find /usr/lib/ -name '__pycache__' -print0 | xargs -0 -n1 rm -rf \
	&& find /usr/lib/ -name '*.pyc' -print0 | xargs -0 -n1 rm -rf

WORKDIR /work
ENTRYPOINT ["flake8"]
CMD ["--version"]
